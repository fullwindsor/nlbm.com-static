var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    cssmin      = require('gulp-cssmin'),
    notify      = require('gulp-notify'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify      = require('gulp-uglify'),
    rename      = require('gulp-rename'),
    sourcemaps  = require('gulp-sourcemaps'),
    babel       = require('gulp-babel'),
    concat      = require('gulp-concat');

var path = {
    sass:       './assets-centennial/scss',
    css:        './css-centennial',
    js:         './assets-centennial/js',
    jsTheme:    './assets-centennial/js/theme/*.js',
}

let autoprefixBrowsers = ['IE 10', 'IE 11'];

gulp.task('js', async function() {
    return gulp.src(path.jsTheme)
        .pipe(concat('theme.js').on('error', function(err) {
            console.log(err);
        }))
        .pipe(uglify().on('error', function(err) {
            console.log(err);
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./js-centennial'))
});

gulp.task('css', async function() {
    return gulp.src(path.sass + '/*.scss')
        .pipe(sass().on('error', function(err) {
            console.log(err);
        }))
        .pipe(autoprefixer(autoprefixBrowsers))
        .pipe(cssmin().on('error', function(err) {
            console.log(err);
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css-centennial'))
});

gulp.task('watch', function() {
    gulp.watch(path.sass + '/**/**/**/*.scss', gulp.series('css'));
    gulp.watch(path.js + '/**/**/*.js', gulp.series('js'));
});

gulp.task('default', gulp.parallel('js', 'css'));